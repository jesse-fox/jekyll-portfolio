[![Netlify Status](https://api.netlify.com/api/v1/badges/fcf495ef-29eb-475f-9fb0-61d338e54a99/deploy-status)](https://app.netlify.com/sites/jane-fox-portfolio/deploys)


# About

Static site built with Jekyll, deployed over Netlify. This is the portfolio for all my web work, so that people can know I am a real and professional person. 


## Requirements

You must have Ruby, Gem, NPM, and Bundler installed.

Quick setup for linux:
```
sudo apt update
sudo apt install -y ruby ruby-dev ruby-bundler libxml2-dev
sudo gem install eventmachine nokogiri
```

## Installing

After cloning the project with git, open a terminal in the folder.

Before running you must install it. Do so with:
```
npm install
bundle install
```


## Running 

Then to start running the site, as well as auto-build the scss, use:
```
bundle exec jekyll serve
```
