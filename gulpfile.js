
// For building assets
var gulp = require('gulp');
var sass = require('gulp-sass');
var minify = require('gulp-csso');
var watch = require('gulp-watch');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');

// For running jekyll
var exec = require('child_process').exec;


var js_dir = [
	"assets/js/jquery.js",
	"assets/js/navigation.js",
	"assets/js/bootstrap.min.js",
	"assets/js/spinhex.js",
	"assets/js/lightbox.js"
];

var js_output = "assets/js/scripts.min.js";



gulp.task('default', ["scripts"]);

gulp.task('watch', function() {

	gulp.watch(js_dir, ['scripts']);

});


gulp.task('scripts', function() {

  return gulp.src(js_dir)
    .pipe(sourcemaps.init())
    .pipe(concat(js_output))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest("./"))
	;

});




gulp.task('jekyll', () => {

  let command = '';

    command = 'jekyll build';
    //command = 'jekyll build --quiet --future --config _config.yml,_config.dev.yml';


  exec('bundle exec jekyll build', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    //cb(err);
  });

});


