

// Start script on page load
document.addEventListener( 'DOMContentLoaded', function( event ) {

	var target = "hex-container";
	var container = document.getElementById( target );

	// Only start script if element is found.
	if ( container ) {
		new hex_placer({target: target});
	}

});


function hex_placer( options ) {
	var self = this;

	self.init = function() {

		//Set defaults for options
		options       = options || {};
		self.target   = options.target || "hex-container";
		self.img_src  = options.img || "/assets/img/hex-part.svg";
		self.height   = options.height || 400;
		self.width    = options.width || window.innerWidth;
		self.hex_size = options.hex_size || 75;

		// Preload hex image
		self.img = document.createElement("img");
		self.img.src = self.img_src;

		// The image being used is 250 wide for 100px side. 
		// This proportion may need to change if you change the image
		self.img.height = self.hex_size * 2.5;
		self.img.width = self.hex_size * 2.5;

		self.container = document.getElementById( options.target );

		self.set_size();

		window.addEventListener('resize', self.set_size);

	}; // init()

	
	// Set up area we want to print to
	self.set_size = function() {

		self.width =  window.innerWidth;

		// Calculate how many we'll need to fill the entire area
		self.fill_x =  self.width / (self.hex_size * 1.5);
		self.fill_y =  self.height / (self.hex_size * 1.5);

		self.container.innerHTML = "";

		self.place_hexes();

	}; // resize()


	// Copies image and places as a hex grid
	self.place_hexes = function() {

		// Go from eg (-1, -1) to (3,3);
		for ( x = -1; x < self.fill_x; x++ ) {

			for ( y = -1; y < self.fill_y; y++ ) {

				var pos = self.get_pos( x, y );

				var new_img = self.img.cloneNode();

				new_img.style = "left: "+ pos.x + "px; top: " + pos.y + "px;";

				self.container.appendChild(new_img);

			}

		}

	}; // place_hexes()


	// converts simple positions (eg {2,1}) to pixels 
	self.get_pos = function( x, y ) {

		var pixel_x = self.hex_size * 1.75 * x;
		var pixel_y = self.hex_size * 1.5 * y;

		// For every odd row, add an offset
		if ( y % 2 === 0 ) {
			pixel_x = pixel_x - ( self.hex_size * .875 );
		}

		var round_x = Math.round(pixel_x);
		var round_y = Math.round(pixel_y);

		return { x: round_x, y: round_y };

	}; // get_pos()

	// Run init when created
	self.init();

} // hex_placer
