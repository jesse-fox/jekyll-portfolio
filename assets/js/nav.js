
let Navigation = function () {

	// These are available in all functions
	let header = document.getElementById("site-header");
	let menu_toggle = document.getElementById("menu-toggle");
  
  
	// run me to to begin
	this.init = function () {
  
	  bind_events();
  
	}; // init()
  
    // Open the menu by remove class
	let open_nav = function () {
  
	  header.classList.add("active");
  
	}; // open_nav()
  

	// Close the menu by remove class
	let close_nav = function () {
  
	  header.removeAttribute("class");
  
	}; // close_nav()


	// Open if closed, close if open
	let toggle_nav = function () {
		console.log("togglin");
		if ( header.classList.contains("active") ) {
			close_nav();
		} else {
			open_nav();
		}
	
	}; // toggle_nav()
		
  
	// React to mouse/touch and window resizes
	let bind_events = function () {
	  menu_toggle.addEventListener('click', toggle_nav);
	  window.addEventListener('resize', close_nav);
	};
  
  }; // Junum
  
  
  // Run when page is loaded
  document.addEventListener("DOMContentLoaded", function (event) {
  
	let nav = new Navigation();
	nav.init();
  
  });
  
  
  